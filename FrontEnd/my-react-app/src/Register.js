import React from 'react';
import './Register.css'

function Register() {
  return (
    <div>
      <form action="" id="register">
        <h1>Registration Form</h1>
        <div className="input_ele">
          <input type="text" placeholder="Firstname" />
          <input type="text" placeholder="Lastname" />
          <input type="email" placeholder="Email" required />
          <input type="password" placeholder="Password" />
        </div>
        <div className="context">
          <input type="checkbox" />
          <span>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis voluptate natus, maiores odio vero nihil tempore dolorum, doloremque provident sed nemo
          </span>
        </div>

        <div className="context">
          <input type="checkbox" />
          <span>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum nisi delectus mollitia assumenda, non ea dolorum maiores quo voluptatibus, sunt iusto doloribus
          </span>
        </div>

        <div className="register_btn">
          <button>Register</button>
        </div>
      </form>
    </div>
  );
}

export default Register;